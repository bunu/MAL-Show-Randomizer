# MAL-Show-Randomizer
Randomizes shows from a text file or the "Plan to Watch" list and then pulls info from MyAnimeList. It also allows you to update your MAL list using the show
that it randomizes, and it can also remove shows from the text file you used as the input if you select the option.

[The MAL API was used with the Java MAL API Wrapper that I wrote.](https://gitlab.com/bunu/MAL-API-Java-Wrapper)

Here are some screenshots from the application.

![alt text](https://i.imgur.com/mZVtuYV.png)
![alt text](https://i.imgur.com/9KNrjis.png)
![alt text](https://i.imgur.com/9XfEeUL.png)
